### stylish-haskell
`.stylish-haskell.yaml` - config file for [stylish-haskell](https://github.com/jaspervdj/stylish-haskell)

```
stylish-haskell -c ~/.stylish-haskell.yaml [FILENAME]
```

### vscode
`settings.json`, `keybindings.json` & `launch.json` (debug config)

###### launch.json
`--with-ghc=haskell-dap`

### hlint
`.hlint.yaml` for hlint rules
[GHCi integration](https://github.com/ndmitchell/hlint#ghci-integration)

### ghci options
`.ghci`
